/*
Name: Christopher Hurst
Class: CSC240
Due Date: 06/19/16
Files: BuzyBot_Calculator.class, BuzyBot_Calculator.java
Description: This program prompts the user to enter two number and then asks the 
    user if they want to add, subtract, multiply, or divide. The program once 
    commanded will give the answer to the user.
 */
package round_object_calculator;

import java.util.Scanner;
import java.text.DecimalFormat;

public class Round_Object_Calculator {

    
     
    public static void main(String[] args) {
    
    Scanner input = new Scanner(System.in);
    
     
//The start of the repeat statement
    
    String redo = "";
    System.out.println("Welcome to the Round Object Calculator");
    System.out.println("This program will calculate the area of a circle or ");
    System.out.println("    the volume of a sphere.");
    System.out.println("The calculations will be based of the user input radius");
    do{
        double radius = 0;
// Asks the user to input their data    
        String data = choice();
        switch (data)  {
            case "S":
                System.out.print("\nThank you. What is the radius of your sphere (in inches): ");
                radius = input.nextDouble();
                double volume = isSphere(radius);
                print(radius, 0, volume);
                break;
            case "C":
                System.out.print("\nThank you. What is the radius of your circle (in inches): ");
                radius = input.nextDouble();
                double area = isCircle(radius);
                print(radius, area, 0);
                break;
            default:
                System.out.println(data + " was not a valid input.");           
        }
// Finishes the repeat statment
       redo = redo();
    }while(redo.equals("Y"));
    
        goodbye();
    }

static String choice(){
    Scanner input = new Scanner(System.in);
    System.out.print("\nEnter C for Circle or S for Sphere: ");
    String data = input.next(); 
    data = data.toUpperCase();
    return data;
}
static double isSphere(double rad){
    double volume = (1.333) * Math.PI * Math.pow(rad, 3);
    return volume;
}

static double isCircle(double rad){
    double area = Math.PI * Math.pow(rad, 2);
    return area;
}

static String redo(){
    Scanner input = new Scanner(System.in);
    System.out.print("\nWould you like to calculate another round object? (Y/N): ");
    String redo = input.next();
    redo = redo.toUpperCase();
    return redo;
}

static void print(double rad, double ar, double vol){
    DecimalFormat DF = new DecimalFormat("0.000");
    if(ar == 0){
    System.out.println("\nThe volume of the sphere with a radius of " 
        + rad + " inches is: " +  DF.format(vol) + "inches cubed");
    } else
    System.out.println("\nThe area of the circle with a radius of " 
        + rad + " inches is: " +  DF.format(ar));
}

static void goodbye(){
    System.out.println("\nThank you for using the Round Object Calculator, Goodbye.");           
}
}
